<?php
  require_once 'db/db.php';
// define variables and set to empty values
$nameErr = $emailErr = $address1Err = $cityErr = $stateErr = $zip_codeErr = $countryErr = "";
$fname = $lname = $address1 = $address2 = $city = $state = $zip_code = $country = "";

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["fname"])||empty($_POST["lname"])) {
    $nameErr = "Name is required";
    echo $nameErr;
  } else {
    $fname = test_input($_POST["fname"]);
    $lname = test_input($_POST["lname"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$fname)||!preg_match("/^[a-zA-Z ]*$/",$lname)) {
      $nameErr = "Only letters and white space allowed";
    }
  }

  if (empty($_POST["address1"])) {
    $address1Err = "Primary Address is required";
  } else {
    $address1 = test_input($_POST["address1"]);

    if (!empty($_POST["address2"])) {
      $address2 = test_input($_POST["address2"]);
    }
  }

  if (empty($_POST["city"])) {
    $cityErr = "City is required";
  } else {
    $city = test_input($_POST["city"]);
  }

  if (empty($_POST["state"])) {
    $stateErr = "State is required";
  } else {
    $state = test_input($_POST["state"]);
  }

  if (empty($_POST["zip_code"])) {
    $zip_codeErr = "Zip code is required";
  } else {
    $zip_code = test_input($_POST["zip_code"]);
  }

  $country = $_POST["country"];
  $sql = "INSERT INTO registered_users (FirstName, LastName, Address1, Address2, City, State, Zip, Country) VALUES ('$fname', '$lname', '$address1', '$address2', '$city', '$state', '$zip_code', '$country')";
  $result = $conn->query($sql);

  echo "Thank you for submitting!";

}

?>