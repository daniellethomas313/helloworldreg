CREATE TABLE IF NOT EXISTS `registered_users`  (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`FirstName` varchar(255),
	`LastName` varchar(255),
	`Address1` varchar(255),
	`Address2` varchar(255) DEFAULT NULL,
	`City` varchar(255),
	`State` varchar(2),
	`Zip` int(5),
	`Country` varchar(2),
	`Date` timestamp
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
