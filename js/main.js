$(document).ready(function(){
	var $state = $("select[name='state']").val();
	var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            $("select[name='city']").html(xmlhttp.responseText);
        }
    };
    xmlhttp.open("GET", "includes/data.php?action=getcities&state=" + $state);
    xmlhttp.send();
});
$("select[name='state']").change(function(){
	var $state = $(this).val();
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            $("select[name='city']").html(xmlhttp.responseText);
        }
    };
    xmlhttp.open("GET", "includes/data.php?action=getcities&state=" + $state);
    xmlhttp.send();    
});