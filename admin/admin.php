<?php 
	define('__ROOT__', dirname(dirname(__FILE__))); 
	require __ROOT__.'/db/db.php'; 
?>
<!DOCTYPE html>
<html>
<head>
	<title>Driver: One Car at a Time</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body id="admin">
<section id="hd">
	<header>
		<nav>
			<a class="align-left" href="/helloworldreg/index.php"><img src="../img/logo-nav.png"></a>
		</nav>
	</header>
</section>
<main id="main">
<?php
	$sql = "SELECT * From registered_users ORDER BY 'Date' DESC";  
	$result = $conn->query($sql);

	echo "<table>
	<tr class='hd-blue align-left'>
	<th>First Name</th>
	<th>Last Name</th>
	<th>Address1</th>
	<th>Address2</th>
	<th>City</th>
	<th>State</th>
	<th>Zip</th>
	<th>Country</th>
	<th>Date</th>
	</tr>";
	while($row = mysqli_fetch_array($result)) {
	    echo "<tr>";
	    echo "<td>" . $row['FirstName'] . "</td>";
	    echo "<td>" . $row['LastName'] . "</td>";
	    echo "<td>" . $row['Address1'] . "</td>";
	    echo "<td>" . $row['Address2'] . "</td>";
	    echo "<td>" . $row['City'] . "</td>";
	    echo "<td>" . $row['State'] . "</td>";
	    echo "<td>" . $row['Zip'] . "</td>";
	    echo "<td>" . $row['Country'] . "</td>";
	    echo "<td>" . $row['Date'] . "</td>";
	    echo "</tr>";
	}
	echo "</table>";
?>
</main>
</body>
</html>