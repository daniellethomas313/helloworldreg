<?php 
	require 'includes/validation.php'; 
	require 'includes/states.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Driver: One Car at a Time</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<section id="hd">
	<header>
		<nav>
			<a class="float-left" href="#"><img src="img/logo-nav.png"></a>
			<a class="align-right" href="admin/admin.php">Admin Report</a>
		</nav>
	</header>
</section>
<main id="main">
	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" accept-charset="utf-8">
		<label for="fname">First Name <span class="notice text-red"><?php echo $nameErr; ?></span></label>
		<input type="text" name="fname" value="<?php echo $fname;?>"required>
		<label for="lname">Last Name <span class="notice text-red"><?php echo $nameErr; ?></span></label>
		<input type="text" name="lname" value="<?php echo $lname;?>" required>
		<label for="address1">Address 1 <span class="notice text-red"><?php echo $address1Err; ?></span></label>
		<input type="text" name="address1" value="<?php echo $address1;?>" required>
		<label for="address2">Address 2</label>
		<input type="text" name="address2" value="<?php echo $address2;?>">
		<label for="city">City <span id="hint"></span> <span class="notice text-red"><?php echo $cityErr; ?></span></label>
		<select name="city" required>
		</select>
		<label for="state">State</label>
		<select name="state">
			<?php 
				foreach ($states as $key => $value) {
					echo '<option value="'.$key.'">'.$value.'</option>';
				}
			?>
		</select>
		<label for="zip_code">Zip Code <span class="notice text-red"><?php echo $zip_codeErr; ?></span></label>
		<input type="text" name="zip_code" value="<?php echo $zip_code;?>" maxlength="5" required>
		<label for="country">Country</label>
		<select name="country" disabled>
			<option value="US" selected>United States</option>
		</select>
		<input type="submit" value="Let's Go!">
	</form>
</main>
<script src="js/main.js"></script>
</body>
</html>